﻿using AlfaBankTestTask.WebApplication.Services;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;

namespace AlfaBankTestTask.UnitTests
{
    /// <summary>
    /// Test suite for GetProducts method of AlfaBankProxy class
    /// </summary>
    [TestFixture]
    public class TestGetProductMethod
    {
        private int productsCount;
        
        [OneTimeSetUp]
        public async Task SetUp()
        {
            await AlfaBankProxy.DownloadAllFromApi();
            productsCount = AlfaBankProxy.GetProductsInfo().ProductsCount;
        }

        
        // Pass zero to both method parameters
        [Test]
        public void TestZeroSkipZeroTake()
        {
            if (productsCount == 0)
            {
                Assert.AreEqual(null, AlfaBankProxy.GetProducts(0,0));
            }
            else
            {
                Assert.AreEqual(0, AlfaBankProxy.GetProducts(0, 0).Count());
            }
        }

        // Pass negative values to both method parameters
        [Test]
        public void TestNegativeSkipNegativeTake()
        {
            if (productsCount == 0)
            {
                Assert.AreEqual(null, AlfaBankProxy.GetProducts(-1, -1));
            }
            else
            {
                Assert.AreEqual(0, AlfaBankProxy.GetProducts(-1, -1).Count());
            }
        }

        // Pass negative skip and positive take to method parameters
        [Test]
        public void TestNegativeSkipPositiveTake()
        {
            if (productsCount == 0)
            {
                Assert.AreEqual(null, AlfaBankProxy.GetProducts(-1, 1));
            }
            else
            {
                Assert.AreNotEqual(0, AlfaBankProxy.GetProducts(-1, 10).Count());
            }
        }

        // Pass positive skip and negative take to method parameters
        [Test]
        public void TestPositiveSkipNegativeTake()
        {
            if (productsCount == 0)
            {
                Assert.AreEqual(null, AlfaBankProxy.GetProducts(1, -1));
            }
            else
            {
                int skip = productsCount - 1;
                Assert.AreEqual(0, AlfaBankProxy.GetProducts(skip, -1).Count());
            }
        }

        // Pass skip value greater than products count to method parameters
        [Test]
        public void TestSkipGreaterProductsCount()
        {
            if (productsCount == 0)
            {
                Assert.AreEqual(null, AlfaBankProxy.GetProducts(productsCount + 1, 10));
            }
            else
            {
                int skip = productsCount + 1;
                Assert.AreEqual(0, AlfaBankProxy.GetProducts(skip, 10).Count());
            }
        }

        // Pass take value greater than products count to method parameters
        [Test]
        public void TestTakeGreaterProductsCount()
        {
            if (productsCount == 0)
            {
                Assert.AreEqual(null, AlfaBankProxy.GetProducts(0, productsCount + 1));
            }
            else
            {
                int take = productsCount + 10;
                Assert.AreEqual(productsCount, AlfaBankProxy.GetProducts(0, take).Count());
            }
        }
    }
}
