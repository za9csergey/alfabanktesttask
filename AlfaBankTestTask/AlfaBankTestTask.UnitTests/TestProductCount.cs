﻿using AlfaBankTestTask.WebApplication.Services;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;

namespace AlfaBankTestTask.UnitTests
{
    /// <summary>
    /// Test suite to check products count after data loaded from external api
    /// </summary>
    [TestFixture]
    public class TestProductCount
    {
        
        [Test]
        public void TestProductsCountOnPage()
        {
            string apiUrl = "http://alfa-test-api.dev.kroniak.net/api/v1/products/";
            // test first page
            AlfaBankApiContext firstPage = AlfaBankProxy.ProcessApiPage(apiUrl);
            if (firstPage.Count == 0)
            {
                Assert.AreEqual(null, firstPage.Products);
            }
            else
            {
                Assert.AreEqual(firstPage.Count, firstPage.Products.Count());
            }
        }

        [Test]
        public async Task TestAllProductsCount()
        {
            await AlfaBankProxy.DownloadAllFromApi();
            if (AlfaBankProxy.GetProductsInfo().ProductsCount == 0)
            {
                Assert.AreEqual(null, AlfaBankProxy.GetProducts(0, int.MaxValue));
            }
            else
            {
                Assert.AreEqual(AlfaBankProxy.GetProductsInfo().ProductsCount, AlfaBankProxy.GetProducts(0, int.MaxValue).Count());
            }
        }
    }
}
