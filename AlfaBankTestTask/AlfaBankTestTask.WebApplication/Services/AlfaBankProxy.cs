﻿using AlfaBankTestTask.WebApplication.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace AlfaBankTestTask.WebApplication.Services
{
    /// <summary>
    /// Store list of products fro external API
    /// </summary>
    public class AlfaBankProxy
    {
        private static string apiUrl = "http://alfa-test-api.dev.kroniak.net/api/v1/products/";

        private static List<Product> products;

        /// <summary>
        /// Proccess all pages from api
        /// </summary>
        /// <returns>List of products</returns>
        public static async Task DownloadAllFromApi()
        {
            await Task.Run(() =>
            {
                products = new List<Product>();
                string url = apiUrl;
                do
                {
                    try
                    {
                        // get api context
                        AlfaBankApiContext context = ProcessApiPage(url);
                        // add products
                        products.AddRange(context.Products.ToList());
                        // go to next url
                        url = context.NextApi;
                    }
                    catch (NullReferenceException e)
                    {
                        Trace.WriteLine(e.Message);
                        products = null;
                    }
                } while (url != null);
            }
            );
        }

        /// <summary>
        /// Deserialize json from url
        /// </summary>
        /// <param name="url">URL with json string</param>
        /// <returns></returns>
        public static AlfaBankApiContext ProcessApiPage(string url)
        {
            // create client
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    // get string from api
                    var content = client.GetStringAsync(url).Result;

                    // deserialize string into context
                    AlfaBankApiContext result =
                        JsonConvert.DeserializeObject<AlfaBankApiContext>(content);
                    return result;
                }
                catch (ArgumentNullException e)
                {
                    Trace.WriteLine(e.Message);
                }
                catch (HttpRequestException e)
                {
                    Trace.WriteLine(e.Message);
                }
            }
            return new AlfaBankApiContext();
        }

        /// <summary>
        /// Get info about products
        /// </summary>
        /// <returns></returns>
        public static ProductsInfo GetProductsInfo()
        {
            try
            {
                return new ProductsInfo(
                    products.Count,
                    products.Min(p => p.Price),
                    products.Max(p => p.Price),
                    products.Average(p => p.Price)
                    );
            }
            catch (NullReferenceException e)
            {
                Trace.WriteLine(e);
            }
            catch (InvalidOperationException e)
            {
                Trace.WriteLine(e);
            }
            return new ProductsInfo();
        }

        /// <summary>
        /// Get a certain amount of product
        /// </summary>
        /// <param name="countToSkip">count of products to skip</param>
        /// <param name="countToTake">count of product to take</param>
        /// <returns>list of products</returns>
        public static IEnumerable<Product> GetProducts(int countToSkip, int countToTake)
        {
            try
            {
                return products.Skip(countToSkip).Take(countToTake);
            }
            catch (NullReferenceException e)
            {
                Trace.WriteLine(e);
                return null;
            }
        }
    }
}