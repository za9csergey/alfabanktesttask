﻿using AlfaBankTestTask.WebApplication.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace AlfaBankTestTask.WebApplication.Services
{
    /// <summary>
    /// Represents external alfa-bank api json
    /// </summary>
    public class AlfaBankApiContext
    {
        [JsonProperty("count")]
        public int Count { get; set; }
        [JsonProperty("products")]
        public IEnumerable<Product> Products { get; set; }
        [JsonProperty("next")]
        public string NextApi { get; set; }
    }
}