﻿using System.Collections.Generic;
using AlfaBankTestTask.WebApplication.Models;
using System.Linq;

namespace AlfaBankTestTask.WebApplication.Services
{
    /// <summary>
    /// Stores info about product list: charachteristics, count, max, min and avg price
    /// </summary>
    public class ProductsInfo
    {
        public string[] Collumns { get; } =
            typeof(Product).GetProperties().Select(p => p.Name).ToArray();

        public int ProductsCount { get; }
        public double MaxPrice { get; }
        public double MinPrice { get; }
        public double AvgPrice { get; }

        public ProductsInfo(int productsCount, double maxPrice, double minPrice, double avgPrice)
        {
            ProductsCount = productsCount;
            MaxPrice = maxPrice;
            MinPrice = minPrice;
            AvgPrice = avgPrice;
        }

        public ProductsInfo()
        {
        }
    }
}