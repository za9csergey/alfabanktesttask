﻿using AlfaBankTestTask.WebApplication.Services;
using System.Web.Http;

namespace AlfaBankTestTask.WebApplication.Controllers
{
    public class ProductController : ApiController
    {
        // GET: api/Product
        /// <summary>
        /// Get info about products
        /// </summary>
        /// <returns></returns>
        public ProductsInfo Get()
        {
            return AlfaBankProxy.GetProductsInfo();
        }


        /// <summary>
        /// Get a certain amount of product
        /// </summary>
        /// <param name="countToSkip">count of products to skip</param>
        /// <param name="countToTake">count of product to take</param>
        /// <returns>list of products</returns>
        public IHttpActionResult Get(int countToSkip, int countToTake)
        {
            return Ok(AlfaBankProxy.GetProducts(countToSkip, countToTake));
        }
        
    }
}
