﻿var productsTable;
var avgPrice;
var productsCount;
var firstRecordIndex;
var recordsCount;
var upTo10Items;
var downTo10Items;

$(document).ready(
    function () {
        // initialize product table and prduct info
        startUp();

        // click previous 10 items
        upTo10Items.click(function () {
            firstRecordIndex -= recordsCount;
            updateUI();
        });

        // click next 10 items
        downTo10Items.click(function () {
            firstRecordIndex += recordsCount;
            updateUI();
        });
    });

// initialize product table and prduct info
function startUp() {
    // init variable for rows
    firstRecordIndex = 0;
    recordsCount = 10;

    // init buttons
    upTo10Items = $("#upTo10Items");
    downTo10Items = $("#downTo10Items");
    upTo10Items.prop("disabled", true);

    // init products info
    console.log("info loaded");
    getProducts(firstRecordIndex, recordsCount);
}

// get info about products
function getProductsInfo() {
    // send GET request
    $.getJSON("api/product/",
        function(data) {
            if (data === 'undefined') {
                $("#noProductsSpan").text("No products on server");
                $("#quantityValueSpan").text("");
                $("#minPriceSpan").text("-");
                $("#maxPriceSpan").text("-");
                return false;
            }
            
            // set statistics
            $("#quantityValueSpan").text(data.ProductsCount);
            $("#minPriceSpan").text(data.MinPrice);
            $("#maxPriceSpan").text(data.MaxPrice);

            // init variables
            productsCount = data.ProductsCount;
            avgPrice = data.AvgPrice;
            productsTable = $("#productsTable tbody");

            // build products table header
            BuildTableHeader(data.Collumns);
            return false;
        });
}

// get products from service
function getProducts(recordIndex, count) {
    // set data for request
    var requestData = {
        countToSkip: recordIndex,
        countToTake: count
    }

    // request
    $.getJSON("api/product", requestData,
        function (data) {
            if (data === 'undefined') {
                $("#noProductsSpan").text("No products on server");
                return false;
            }

            // process received data
            $.each(data,
                function(key, item) {
                    addNewRow(item);
                });
            return false;
        });
}

// update UI on buttons click
function updateUI() {
    // remove all rows
    productsTable.empty();
    checkFirstRecordIndex();
    getProducts(firstRecordIndex, recordsCount);
}

// check FirstRecordIndex and manage buttons enablity
function checkFirstRecordIndex() {
    if (firstRecordIndex === 0) {
        upTo10Items.prop("disabled", true);
    } else {
        upTo10Items.prop("disabled", false);
    }

    if (firstRecordIndex + recordsCount >= productsCount) {
        downTo10Items.prop("disabled", true);
    } else {
        downTo10Items.prop("disabled", false);
    }
}

// build table header
function BuildTableHeader(collumns) {
    var cells = "";
    for (var key in collumns) {
        cells += "<th>" + collumns[key] + "</th>";
    }    
    var tr = "<tr class=\"headerRow\">" + cells + "</tr>";
    $("#productsTable thead").append(tr);
}

// add to table new row
function addNewRow(product) {
    // build cells
    var cells = buildCells(product);
    // build new row
    var tr = "<tr class=\"dataRow\">" + cells + "</tr>";
    // append to table and save data on client side
    productsTable.append(tr);
    
}

// builds cells
function buildCells(item) {
    var cells = "";
    for (var key in item) {
        var htmlClass = "";
        switch (key) {
            case 'price':
                
                if (item[key] > 1.2 * avgPrice) {
                    htmlClass = "class=\"upperAvgPrice\"";
                }
                if (item[key] < 0.8 * avgPrice) {
                    htmlClass = "class=\"lowerAvgPrice\"";
                }
                break;

        }
        if (key.includes("description")) {
            htmlClass = "class=\"descriptionCell\"";
        }

        cells += "<td "+ htmlClass + ">" + item[key] + "</td>";
    }
    
    return cells;
}