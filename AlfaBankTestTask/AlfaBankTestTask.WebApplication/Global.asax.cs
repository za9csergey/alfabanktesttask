﻿using AlfaBankTestTask.WebApplication.Services;
using System.Web.Http;

namespace AlfaBankTestTask.WebApplication
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected async void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            await AlfaBankProxy.DownloadAllFromApi();
        }
    }
}
